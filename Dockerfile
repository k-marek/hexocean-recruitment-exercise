FROM node:16-alpine3.12 as development

RUN apk add --no-cache git

WORKDIR /app

RUN mkdir node_modules

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
RUN npm install

COPY src ./src
COPY public ./public
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]

FROM development as builder

WORKDIR /app
RUN npm run build

FROM nginx:1.21 as release

COPY --from=builder /app/build/ /usr/share/nginx/html
COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["./entrypoint.sh", "production"]
