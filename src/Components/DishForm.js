import React from "react";
import { connect } from "react-redux";
import {
  Field,
  reduxForm,
  formValueSelector,
  SubmissionError,
} from "redux-form";

async function submitToServer(data) {
  try {
    let response = await fetch(
      "https://frosty-wood-6558.getsandbox.com:443/dishes",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }
    );
    let responseJson = await response.json();
    return responseJson.dish;
  } catch (error) {
    console.log(error);
  }
}

const required = (value) => (value ? undefined : "Required");
const number = (value) =>
  value && isNaN(Number(value)) ? "Must be a number" : undefined;
const minValue = (min) => (value) =>
  value && value < min ? `Must be at least ${min}` : undefined;
const maxValue = (max) => (value) =>
  value && value > max ? `${max} is maximum value` : undefined;
const minValue1 = minValue(1);
const maxValue10 = maxValue(10);
const duration = (value) =>
  value && !/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/i.test(value)
    ? "Invalid value, must be 00:00:00 format"
    : undefined;

const submit = ({
  name,
  preparation_time,
  type,
  no_of_slices,
  diameter,
  spiciness_scale,
  slices_of_bread,
}) => {
  let error = {};
  let isError = false;

  if (isError) {
    throw new SubmissionError(error);
  } else {
    return submitToServer({
      name,
      preparation_time,
      type,
      no_of_slices,
      diameter,
      spiciness_scale,
      slices_of_bread,
    }).then((data) => {
      if (data.error) {
        throw new SubmissionError(data.error);
      }
    });
  }
};

const renderField = ({
  label,
  type,
  input,
  placeholder,
  meta: { touched, error },
}) => (
  <div className="input-row">
    <label>{label}</label>
    <input {...input} type={type} placeholder={placeholder} />
    {touched && error && <span className="error">{error}</span>}
  </div>
);

let DishForm = (props) => {
  const { handleSubmit, type, submitting } = props;
  return (
    <form onSubmit={handleSubmit(submit)}>
      <div className="form-background"></div>
      <div className="form">
        <div className="form-content">
          <h1>Dish Form</h1>
          <div>
            <Field
              name="name"
              label="Dish Name"
              component={renderField}
              type="text"
              validate={required}
            />
          </div>
          <div>
            <Field
              name="preparation_time"
              label="Preparation Time"
              component={renderField}
              type="text"
              placeholder="00:00:00"
              required
              pattern="[0-9]{2}:[0-9]{2}:[0-9]{2}"
              value="00:00:00"
              validate={[required, duration]}
            />
          </div>
          <div>
            <label htmlFor="type">Dish Type</label>
            <Field name="type" id="type" component="select">
              <option />
              <option value="pizza" name="pizza">
                Pizza
              </option>
              <option value="soup" name="soup">
                Soup
              </option>
              <option value="sandwich" name="sandwich">
                Sandwich
              </option>
            </Field>
          </div>
          {type === "pizza" && (
            <div>
              <div>
                <Field
                  name="no_of_slices"
                  label="No of slices"
                  component={renderField}
                  type="number"
                  parse={(val) => parseInt(val, 10)}
                  validate={number}
                />
              </div>
              <div>
                <Field
                  name="diameter"
                  label="Diameter"
                  component={renderField}
                  type="number"
                  step="0.1"
                  parse={(val) => parseInt(val, 10)}
                  validate={number}
                />
              </div>
            </div>
          )}
          {type === "soup" && (
            <div>
              <div>
                <Field
                  name="spiciness_scale"
                  label="Spiciness scale"
                  component={renderField}
                  type="number"
                  placeholder="1-10"
                  parse={(val) => parseInt(val, 10)}
                  validate={[number, minValue1, maxValue10]}
                />
              </div>
            </div>
          )}
          {type === "sandwich" && (
            <div>
              <div>
                <Field
                  name="slices_of_bread"
                  label="Slices of bread"
                  component={renderField}
                  type="number"
                  inputMode="numeric"
                  parse={(val) => parseInt(val, 10)}
                  validate={number}
                />
              </div>
            </div>
          )}
          <button type="submit" disabled={submitting}>
            Submit
          </button>
        </div>
      </div>
    </form>
  );
};

DishForm = reduxForm({
  form: "dish",
})(DishForm);

const selector = formValueSelector("dish");
DishForm = connect((state) => {
  const type = selector(state, "type");
  return {
    type,
  };
})(DishForm);

export default DishForm;
