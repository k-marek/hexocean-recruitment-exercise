import React from "react";
import "./App.css";

import DishForm from "./Components/DishForm";

function App() {
  const submit = (values) => {
    console.log(values);
  };
  return (
    <div>
      <DishForm onSubmit={submit} />
    </div>
  );
}

export default App;
